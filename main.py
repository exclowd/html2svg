import pypandoc
import os
import sys
import subprocess
import shlex
import re
from tempfile import TemporaryDirectory
from ctypes.util import find_library

default_template = r"""
\documentclass[{{ fontsize }}pt,preview]{standalone}
{{ preamble }}
\begin{document}
\begin{preview}
{{ code }}
\end{preview}
\end{document}
"""

default_preamble = r"""
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
"""

latex_cmd = 'latex -interaction nonstopmode -halt-on-error'
dvisvgm_cmd = 'dvisvgm --no-fonts'

params = {
    'fontsize': 12,  # pt
    'template': default_template,
    'preamble': default_preamble,
    'latex_cmd': latex_cmd,
    'dvisvgm_cmd': dvisvgm_cmd,
    'libgs': None,
}

def latex2svg(code, working_directory=None, name='code'):
    """Convert LaTeX to SVG using dvisvgm.

    Parameters
    ----------
    code : str
        LaTeX code to render.
    params : dict
        Conversion parameters.
    working_directory : str or None
        Working directory for external commands and place for temporary files.

    Returns
    -------
    dict
        Dictionary of SVG output and output information:

        * `svg`: SVG data
        * `width`: image width in *em*
        * `height`: image height in *em*
        * `depth`: baseline position in *em*
    """

    latex_file = name + '.tex'
    dvi_file = name + '.dvi'
    log_file = name + '.log'
    aux_file = name + '.aux'

    if working_directory is None:
        with TemporaryDirectory() as tmpdir:
            return latex2svg(code, working_directory=tmpdir)

    fontsize = params['fontsize']
    document = (params['template']
                .replace('{{ preamble }}', params['preamble'])
                .replace('{{ fontsize }}', str(fontsize))
                .replace('{{ code }}', code))

    with open(os.path.join(working_directory, latex_file), 'w') as f:
        f.write(document)

    # Run LaTeX and create DVI file
    try:
        ret = subprocess.run(shlex.split(params['latex_cmd'] + ' ' + latex_file),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                             cwd=working_directory)
        ret.check_returncode()
    except FileNotFoundError:
        raise RuntimeError('latex not found')

    # Add LIBGS to environment if supplied
    env = os.environ.copy()
    if params['libgs']:
        env['LIBGS'] = params['libgs']



    # Convert DVI to SVG
    try:
        ret = subprocess.run(shlex.split(params['dvisvgm_cmd'] + " " + dvi_file),
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                             cwd=working_directory, env=env)
        ret.check_returncode()
    except FileNotFoundError:
        raise RuntimeError('dvisvgm not found')

    os.remove(os.path.join(working_directory,latex_file))
    os.remove(os.path.join(working_directory,log_file))
    os.remove(os.path.join(working_directory,aux_file))
    os.remove(os.path.join(working_directory,dvi_file))

    # Parse dvisvgm output for size and alignment
    def get_size(output):
        regex = r'\b([0-9.]+)pt x ([0-9.]+)pt'
        match = re.search(regex, output)
        if match:
            return (float(match.group(1)) / fontsize,
                    float(match.group(2)) / fontsize)
        else:
            return None, None

    def get_measure(output, name):
        regex = r'\b%s=([0-9.e-]+)pt' % name
        match = re.search(regex, output)
        if match:
            return float(match.group(1)) / fontsize
        else:
            return None

    output = ret.stderr.decode('utf-8')
    width, height = get_size(output)
    depth = get_measure(output, 'depth')
    return {'depth': depth, 'width': width, 'height': height}

f = open("input.txt", 'r')

cnt = 0
while True:
    cnt += 1
    line = f.readline()
    if not line:
        break
    out = pypandoc.convert_text(line, "latex", "html")
    latex2svg(out, working_directory='./data', name='img'+str(cnt))

